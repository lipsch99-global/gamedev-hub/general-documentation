# Workflow

## Table of Contents
- [Creating Tasks](#creating-tasks)
- [Working on Tasks](#working-on-tasks)
- [Reviewing a Merge Request](#reviewing-a-merge-request)

## Creating Tasks
To read about what a **Task** is in the **Hack'n'Plan** sense checkout their [documentation](https://hacknplan.com/knowledge-base/anatomy-of-a-task/).  

To create a new **Task** in **Hack'n'Plan** go to the **backlog** page, click on the ***Add item*** button and select **Task**.  

### Title
Try to give your new **task** a fitting name. For example:  
```Player Movement```
### Board
For the board you first choose ***No board***. Later we can discuss when to work on what **task**.

## Description
In the description you write down what you want to be done in this task. Also describe how you want it to be achieved.

## Tags
Add the correct tag to it.  
```Player``` for stuff regarding the player. Like a new mechanic or a gameplay fix.  
```GameDev``` for stuff regarding the developer. A new developer plugin, or documentation.
### Assigned users
Clear the assigned user (yourself) from the list.  
Which user does what work is discussed later, when we pick up the task.

### Sub Tasks
You will want to add some **sub tasks** to the task to have smaller packets that you can work on.  
To do that select the ***Fields*** Dropdown on the top of the window and enable ***Subtasks***.

### Example

Here you see a example of a **Task**:
![Task Creation](/Documentation/TaskCreation.png)

## Working on Tasks

1. First you assing yourself the new **task** in **Hack'n'Plan** and move it to ***IN PROGRESS***.
![Assign Task](/Documentation/AssignTask.png)

2. Switch to new **Branch**.  
    In console you do it as follows:

    ``` shell
    git checkout feat/PlayerMovement
    ```
    The naming should be like this:  
    ***type of work/name of branch***

    *type of branch* could be:
    - **feat:** if you add a new feature
    - **docs:** if you add some sort of documentation
    - **chore:** if you do some smaller things that the end user won't really notice 
    - **fix:** if you fix something

3. Start working
4. Commit regularly. You commit messages should look similar to the branch naming like this:  
        ***type of work: what did you do***

5. After your first commit create a **Merge Request** on **GitLab**.  
    1. Go to ***Merge requests*** and click on ***New merge request***
    2. Select your **Branch** as source branch
    3. Click ***Compare branches and continue***
    4. Add the ***WIP*** tag to the **Title** if you're still working on the **Merge Request**.
    5. Add an **Assignee** (you if you're still working on it, the reviewer if you're done)
    6. Add a tick to both the checkboxes at the bottom. (**Squash commits** & **Delete source branch**)
    7. Click ***Submit merge request***.

6. When done, move the **Task** in **Hack'n'Plan** to in Test and assign the reviewer as **Asignee**.  


## Reviewing a Merge Request

When a **Merge Request** has been assigned to you, you have to review it for **cleanness** and **quality** of the code and if it makes **sense**.

To do that you switch to the ***Changes*** tab in the **merge request**.

Now you can go through all the changed files and look at the changes. If you see something that bothers you, move the mouse to that line, press the **comment bubble** symbol and write down your thoughts.  

If you see something that you think that person did very well feel free to also write a comment on it. It help to not only see the mistakes you made, but also what you did great.  

![Suggestion Review](/Documentation/SuggestionReview.png)

