# Game Development General Documentation

## Workflow
In this chapter you'll read about how to work with **Hack'n'Plan** and GitLab while developing a game in a **Team**.
Read about it [here](Documentation/workflow.md).